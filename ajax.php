<?php

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405);
    exit('Method Not Allowed');
}

$firstName = $_POST['firstName'] ?? '';
$lastName = $_POST['lastName'] ?? '';
$email = $_POST['email'] ?? '';
$password = $_POST['password'] ?? '';
$confirmPassword = $_POST['confirmPassword'] ?? '';

if (empty($firstName)) {
    $response = [
        'status' => 'error',
        'type' => 'name',
        'message' => 'Всі поля мають бути заповнені.'
    ];
    echo json_encode($response);
    exit;
}
if (empty($password) || empty($confirmPassword)) {
    $response = [
        'status' => 'error',
        'type' => 'password',
        'message' => 'Будь ласка, введіть пароль.'
    ];
    echo json_encode($response);
    exit;
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $response = [
        'status' => 'error',
        'type' => 'email',
        'message' => 'Будь ласка, введіть правильну адресу електронної пошти.'
    ];
    echo json_encode($response);
    exit;
}
if ($password != $confirmPassword) {
    $response = [
        'status' => 'error',
        'type' => 'password',
        'message' => 'Пароль та підтвердження пароля не збігаються.'
    ];
    echo json_encode($response);
    exit;
}

if (file_exists('users.json')) {
    $usersData = file_get_contents('users.json');
    $users = json_decode($usersData, true);
}else{
    $users = [];
}
$users = json_decode($usersData, true);
foreach ($users as $user) {
    if ($user['email'] == $email) {
        $response = [
            'status' => 'error',
            'type' => 'email',
            'message' => 'Користувач з таким емейлом вже існує.'
        ];
        echo json_encode($response);
        exit;        
    }
}

$hashedPassword = md5("JustTest" . $password);
$newUser = [
    'firstName' => $firstName,
    'lastName' => $lastName,
    'email' => $email,
    'password' => $hashedPassword
];
$users[] = $newUser;
file_put_contents('users.json', json_encode($users));

$response = [
    'status' => 'success',
    'type' => '',
    'message' => 'Дякуємо за реєстрацію!'
];
echo json_encode($response);
exit;
